from django import forms
from .models import StatusModel

class StatusForm(forms.Form):
    Status = forms.CharField(max_length = 300, label="", widget=forms.TextInput(attrs={'placeholder': "What's your status?"}))
