from django.test import TestCase, Client, LiveServerTestCase
from .models import StatusModel
from .views import Index6
from .forms import StatusForm
from django.utils import timezone
from django.urls import resolve
from datetime import datetime
from unittest import mock
from selenium import webdriver
import time
from selenium.webdriver.chrome.options import Options


class Story6UnitTest(TestCase):

    def test_Story6_url_is_exist(self):
        response = Client().get('/my-status/')
        self.assertEqual(response.status_code, 200)
    
    def test_Story6_func(self):
        found = resolve('/my-status/')
        self.assertEqual(found.func, Index6)

    def test_Story6_to_do_list_template(self):
        response = Client().get('/my-status/')
        self.assertTemplateUsed(response, 'index6.html')
    
    def test_Story6_input_status(self):
        response = Client().post('/my-status/' , {'Status' : 'yeay'})
        hitung = StatusModel.objects.all().count()
        self.assertEqual(hitung, 1)
    
    def test_Story6_model_saving_status_object_should_count(self):
        new_activity = StatusModel.objects.create(Date=timezone.now(), Status='gembira')
        counting_all = StatusModel.objects.all().count()
        self.assertEqual(counting_all, 1)
    
    def test_Story6_model_time_now(self):
        now_datetime = datetime.now()
        with mock.patch("django.utils.timezone.now", mock.Mock(return_value = now_datetime)):
            new_status = StatusModel.objects.create(Status='positive vibes')
            self.assertEqual(new_status.Date, now_datetime)
    
    def test_Story6_form_charfield_more_than_300(self):
        check_form = StatusForm({'Status' : "happy"*1000})
        self.assertFalse(check_form.is_valid())

class Story6FuntionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story6FuntionalTest, self).setUp()

    def test_input_status(self):
        self.driver.get("http://localhost:8000/my-status/")
        status_field = self.driver.find_element_by_id('id_Status')
        text_input = "Coba Coba"
        status_field.send_keys(text_input)
        time.sleep(4)
        status_field.submit()
        time.sleep(4)
        p_elements = self.driver.find_elements_by_tag_name('p')
        status_words = p_elements[-2].text.split(":")
        self.assertEqual(status_words[-1].strip(), text_input)

        