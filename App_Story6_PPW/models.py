from django.db import models
from django.core.validators import MaxLengthValidator

# Create your models here.
class StatusModel(models.Model):

    Status = models.CharField(max_length = 300, validators = [MaxLengthValidator(limit_value = 300)])
    Date = models.DateTimeField(auto_now_add= True)

    def __str__(self):
        return self.Status

