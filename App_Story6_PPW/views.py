from django.shortcuts import render, redirect 
from django.http import HttpResponse
from App_Story6_PPW.forms import StatusForm
from App_Story6_PPW.models import StatusModel

def Index6(request):
    semua_status = StatusModel.objects.all()
    sent_data = {"all_status" : semua_status}

    if request.method == "GET":      #Tampilin form-nya
        form = StatusForm()
        sent_data["status_form"] = form #nambahin element ke dict
        return render (request, 'index6.html', sent_data)
    else: 
        isi_form = StatusForm(data=request.POST)     #dict yg isinya data yg telah di submit 
        if isi_form.is_valid():
            new_status = request.POST["Status"]

            new_status_2 = StatusModel(
                Status = new_status,
            )
            new_status_2.save()
            return redirect("/my-status/") #ke halaman hasil form
        else:
            return HttpResponse("NOT SAVED")
            